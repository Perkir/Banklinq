using System;

namespace BankLinq.Models
{
    public class Account
    {
        public int Id { get; set; }
        public User User{ get; set; }
        public DateTime CreationDate { get; set; }
        public float Summ { get; set; }
        
        public override string ToString()
        {
            return $"Id={Id}; Дата= {CreationDate:d}; Сумма ={Summ} ";
        }

    }
}