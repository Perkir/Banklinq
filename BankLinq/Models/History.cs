using System;

namespace BankLinq.Models
{
    public class History
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int Kind { get; set; }
        public float Summ { get; set; }
        public Account Account { get; set; }
        public override string ToString()
        {
            return $"Id={Id}; Дата= {CreationDate:d}; Тип={Kind} Сумма ={Summ} ";
        }
    }
}