﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BankLinq.Models;


namespace BankLinq 
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("приложение, имитирующее работу банкомата");
            /// создаем пользователей
            List<User> users = new List<User>();
            users = CreateUsers();
            /// создаем аккаунты
            List<Account> accounts = new List<Account>();
            accounts = CreateAccounts(users);
            /// Создаем историю
            List<History> histories = new List<History>();
            histories = CreateHistories(accounts);
       
            /// Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
            GetUserInfo(users, accounts, histories);

            /// Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
            GetOperationInfo(histories, accounts);

            ///Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой) 
            GetUserInfoBySum(accounts);
        
            Console.ReadKey();

        }
        private static void GetUserInfoBySum(List<Account> accounts)
        {
            Console.WriteLine("");
            Console.WriteLine("Сумма на счете > N");
            Console.Write("Сумма =");
            string findSum = Console.ReadLine();
            double convertSum;
            double.TryParse(findSum, out convertSum );
            
            foreach (var item in accounts.Where(x => x.Summ > convertSum).Select(x => x.User).Distinct())
            {
                Console.WriteLine(item.ToString());
            }
        }
        private static void GetOperationInfo(List<History> histories, List<Account> accounts)
        {
            Console.WriteLine("");
            Console.WriteLine("История платежей:");

            var hh = from h in histories
                join a in accounts on h.Account.Id equals a.Id
                where h.Kind == 1
                select (a.Id, a.Summ, a.CreationDate, a.User);


            foreach (var h in hh)
            {                
                Console.WriteLine($"Id={h.Id}; Дата={h.CreationDate:d} Сумма={h.Summ} " + h.User.ToString());
            }
        }

        static void GetUserInfo(List<User> users, List<Account> accounts, List<History> histories)
        {
            User u = users.Where(x => x.Login == "1" && x.Password == "1").FirstOrDefault();
            Console.WriteLine("Пользователь = "+u?.ToString());
            Console.WriteLine("Список Аккаунтов:");

            foreach (var item in accounts.Where(x => x.User == u).ToList())
            {
                Console.WriteLine(item.ToString());
            }
            
            Console.WriteLine("");
            Console.WriteLine("Информация о счете:");

            foreach (var a in accounts.Where(x => x.User == u).ToList())
            {
                Console.WriteLine(a.ToString());
                foreach (var h in histories.Where(x => x.Account==a).ToList())
                {
                    Console.WriteLine("    "+h.ToString());
                }
            }
        }

        public static List<History> CreateHistories(List<Account> accounts)
        {   
            List<History> histories = new List<History>();
            histories.Add(new History() { Id = 1, Account = accounts[0], CreationDate = DateTime.Now, Kind = 1, Summ = 50_000 });
            histories.Add(new History() { Id = 2, Account = accounts[0], CreationDate = DateTime.Now, Kind = -1, Summ = -5_000 });
            histories.Add(new History() { Id = 3, Account = accounts[1], CreationDate = DateTime.Now, Kind = 1, Summ = 2_000 });
            histories.Add(new History() { Id = 4, Account = accounts[2], CreationDate = DateTime.Now, Kind = 1, Summ = 700 });
            histories.Add(new History() { Id = 5, Account = accounts[3], CreationDate = DateTime.Now, Kind = 1, Summ = 500_000 });
            return histories;
        }

         public static List<Account> CreateAccounts(List<User> users)
        {
            List<Account> accounts = new List<Account>();
            Account a1 = new Account() { Id = 1, CreationDate = DateTime.Now, Summ = 5_000, User = users[0]};
            Account a2 = new Account() { Id = 2, CreationDate = DateTime.Now, Summ = 2_000, User = users[1] };
            Account a3 = new Account() { Id = 3, CreationDate = DateTime.Now, Summ = 800,   User = users[2] };
            Account a4 = new Account() { Id = 4, CreationDate = DateTime.Now, Summ = 6_000, User = users[0] };
            accounts.Add(a1);
            accounts.Add(a2);
            accounts.Add(a3);
            accounts.Add(a4);
            return accounts;
        }

        public static List<User> CreateUsers()
        {
            List<User> users = new List<User>();
            User u1 = new User() { Id = 1, CreationDate = DateTime.Now, Surname = "Отчество1", Name = "Имя1", Middlename = "Фамилия1", Login = "1", Password = "1", Passport = "Pasport1", Phone = "8-111-111-11-11" };
            User u2 = new User() { Id = 2, CreationDate = DateTime.Now, Surname = "Отчество2", Name = "Имя2", Middlename = "Фамилия2", Login = "2", Password = "2", Passport = "Pasport2", Phone = "8-222-222-22-22" };
            User u3 = new User() { Id = 3, CreationDate = DateTime.Now, Surname = "Отчество3", Name = "Имя3", Middlename = "Фамилия3", Login = "3", Password = "3", Passport = "Pasport3", Phone = "8-333-333-33-33" };
            users.Add(u1);
            users.Add(u2);
            users.Add(u3);
            return users;
        }
        
    }
}